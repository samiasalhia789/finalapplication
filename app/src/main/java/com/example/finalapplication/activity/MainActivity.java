package com.example.finalapplication.activity;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.example.finalapplication.R;
import com.example.finalapplication.adapter.ViewPagerAdapter;
import com.example.finalapplication.fragment.IntroductionFragment;
import com.example.finalapplication.fragment.ProblemsFragment;
import com.example.finalapplication.fragment.TipsFragment;
import com.example.finalapplication.modle.TheTab;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private DrawerLayout Drawer;
    private Toolbar toolbar;
    private NavigationView nvDrawer;
    private ActionBarDrawerToggle drawerToggle;
    private TabLayout tap_layout;

    ArrayList<MenuItem> menuItems = new ArrayList<>();
    int Postion = 0;
    ViewPager viewPager;
    String CHANNEL_ID = "NEW_CHANNEL_ID5";

    private String email;
    SharedPreferences sp;
    TabLayout tabLayout;
    ArrayList<TheTab> fragments;

    TextView Name;
    TextView Email;
    ImageView Image_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.TabLayout);

        tap_layout = findViewById(R.id.TabLayout);
        toolbar = findViewById(R.id.toolbar);
        Drawer = findViewById(R.id.drawer_layout);
        nvDrawer = findViewById(R.id.nvView);

        setSupportActionBar(toolbar);


        drawerToggle = setupDrawerToggle();

        drawerToggle.setDrawerIndicatorEnabled(true);
        Drawer.addDrawerListener(drawerToggle);
        drawerToggle.syncState();



        /*----------------------header------------------------*/
        nvDrawer = findViewById(R.id.nvView);
        View headerView = nvDrawer.inflateHeaderView(R.layout.nav_header);
        Name = headerView.findViewById(R.id.Name);
        Email = headerView.findViewById(R.id.Email);
        Image_user = headerView.findViewById(R.id.Image_user);


        sp = getSharedPreferences("user_name", MODE_PRIVATE);
        Name.setText(sp.getString("Name", ""));
        Email.setText(sp.getString("Email", ""));

//        ReadUsingAsyncTask async = new ReadUsingAsyncTask();
//        async.execute();

//        Glide.with(MainActivity.this)
//                .load(user.getImage_user())
//                .into(Image_user);


        setupDrawerContent(nvDrawer);

        for (int i = 0; i < nvDrawer.getMenu().size(); i++) {
            menuItems.add(nvDrawer.getMenu().getItem(i));
        } // end for

        fragments = new ArrayList<>();
        fragments.add(new TheTab("مقدمة", IntroductionFragment.newInstance("", "")));
        fragments.add(new TheTab("مشاكل", ProblemsFragment.newInstance("", "")));
        fragments.add(new TheTab("نصائح", TipsFragment.newInstance("", "")));

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), fragments);
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(1);


        createNotificationChannel();
        createNotification(sp.getString("Name", ""), 1);


    } // end onCreate

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                selectDrawerItem(menuItem);
                return true;
            } // end onNavigationItemSelected()
        }); // end setNavigationItemSelectedListener()

    } // end setupDrawerContent()

    public void selectDrawerItem(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.about:
                Intent intent1 = new Intent(MainActivity.this, AboutApp.class);
                startActivity(intent1);
                break;

            case R.id.Similar_apps:
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://play.google.com/store/search?q=%D8%B9%D9%84%D9%85%20%D8%A7%D9%84%D9%86%D9%81%D8%B3&hl=ar&gl=US"));
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
                break;

            case R.id.ChangePassword:
                Intent intent3 = new Intent(MainActivity.this, ChangePassword.class);
                startActivity(intent3);
                break;


        } // end switch

        menuItem.setChecked(true);

        nvDrawer.getMenu().getItem(Postion).setChecked(false);
        Postion = menuItems.indexOf(menuItem);
        Drawer.closeDrawers();
    } //end selectDrawerItem()

    public void openFragment(Fragment fragment, int container) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(container, fragment);
        transaction.commit();
    } // end openFragment()

    @Override
    public void onBackPressed() {

        if (Drawer.isDrawerOpen(GravityCompat.START)) {
            Drawer.closeDrawer(GravityCompat.START);
        } // end if
        else {
            super.onBackPressed();
        } // end else

    } // end onBackPressed()


    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this,
                Drawer,
                toolbar,
                R.string.drawer_open,
                R.string.drawer_close);
    } // end setupDrawerToggle()


    private void createNotificationChannel() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "NEW_CHANNEL_NAME5", importance);
            channel.setDescription("DESCRIPTION");
            channel.setSound(null, null);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }


    private void createNotification(String title, int id) {

        Intent intent = new Intent(MainActivity.this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity
                (MainActivity.this, 0, intent, 0);
        NotificationCompat.Builder builder = new NotificationCompat.Builder

                (MainActivity.this, CHANNEL_ID)
                .setSmallIcon(R.drawable.woman)
                .setContentTitle(title)
                .setContentIntent(pendingIntent)
                .setOngoing(false)
                .setStyle(new NotificationCompat.BigTextStyle().bigText("اهلاً و سهلاً بك في نَفَسْ"))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        final Uri NOTIFICATION_SOUND = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        RingtoneManager.getRingtone(MainActivity.this, NOTIFICATION_SOUND).play();
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(MainActivity.this);
        notificationManager.notify(id, builder.build());
    }

//    class ReadUsingAsyncTask extends AsyncTask<Void, Void, User> {
//
//        @Override
//        protected User doInBackground(Void... voids) {
//            User user = DatabaseClient.getInstance(getApplicationContext())
//                    .getAppDatabase()
//                    .userDao()
//                    .getCurrentUseremail(email);
//            return user;
//        }
//
//        @Override
//        protected void onPostExecute(User user) {
//            super.onPostExecute(user);
//
//
//            Glide.with(MainActivity.this)
//                    .load(user.getImage_user())
//                    .into(Image_user);
//
//
//        }
//    }
} // end class MainActivity