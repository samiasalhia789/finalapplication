package com.example.finalapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.finalapplication.database.DatabaseClient;
import com.example.finalapplication.databinding.ActivityChangePasswordBinding;
import com.example.finalapplication.modle.User;


public class ChangePassword extends AppCompatActivity {

    private ActivityChangePasswordBinding binding;

    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityChangePasswordBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


        binding.AboutImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ChangePassword.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        });

        binding.save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                readAndUpdate();
            }
        });// end setOnClickListener
//        readAll();

    } // end onCreate


    private void readAndUpdate() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                if ((!binding.changePasswordEtPassword.getText().toString().isEmpty()
                        && !binding.changePasswordEtNewPassword.getText().toString().isEmpty()
                        && !binding.changePasswordEtPasswordAgain.getText()
                        .toString().isEmpty())) {

                    user = DatabaseClient.getInstance(getApplicationContext())
                            .getAppDatabase()
                            .userDao()
                            .getCurrentUseremail(binding.changePasswordEtPassword.getText().toString().trim());

                    Log.d("TAG", "run:old" + user.getUserPassword());

                    if (binding.changePasswordEtNewPassword.getText().toString().
                            equals(binding.changePasswordEtPasswordAgain.getText().toString())) {

                        user.setUserPassword(binding.changePasswordEtPasswordAgain.getText().toString());

                        Log.d("TAG", "run:" + binding.changePasswordEtPasswordAgain.getText().toString());
                        Log.d("TAG", "run:new" + user.getUserPassword());
                        Intent intent = new Intent(getBaseContext(), LoginActivity.class);
                        startActivity(intent);
                        finish();
                    } // end if2
                    else {

                        Toast.makeText(getBaseContext(), "يجب ان تكون كلمة المرور مطابقة.", Toast.LENGTH_SHORT).show();
                    } // end else 2


                } else {
                    Toast.makeText(getBaseContext(), "يجب ادخال جميع الحقول", Toast.LENGTH_SHORT).show();

                } // end else 2


                Log.e("update", "Done");
//                Log.e("Description",  user.getUserPassword());
//                Log.e("Description",  binding.changePasswordEtPasswordAgain.getText().toString());


            }
        });
        thread.start();
    }

//    private void readAll() {
//        Thread thread = new Thread(new Runnable() {
//            @Override
//            public void run() {
//
//                User users = DatabaseClient.getInstance(getApplicationContext())
//                        .getAppDatabase()
//                        .userDao()
//                        .getPasswordUser(binding.changePasswordEtNewPassword.getText().toString());
//
//
//                Log.d("TAGG", "run: " + binding.changePasswordEtNewPassword.getText().toString());
//
//
//            }
//
//            private void runOnUiThread() {
//                Toast.makeText(getBaseContext(), "readAll" + binding.changePasswordEtNewPassword.getText().toString(), Toast.LENGTH_SHORT).show();
//
//            }
//
//        });
//        thread.start();
//    }
} // end class ChangePassword


