package com.example.finalapplication.activity;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.example.finalapplication.databinding.ActivityAboutAppBinding;

public class AboutApp extends AppCompatActivity {
    private ActivityAboutAppBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAboutAppBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


    }  //end onCreate
} // end class AboutApp