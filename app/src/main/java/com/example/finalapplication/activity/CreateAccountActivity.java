package com.example.finalapplication.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.finalapplication.database.DatabaseClient;
import com.example.finalapplication.databinding.ActivityCreateAccountBinding;
import com.example.finalapplication.modle.User;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class CreateAccountActivity extends AppCompatActivity {
    private ActivityCreateAccountBinding binding;
    Uri imageUri;
    public static final int PICK_IMAGE = 100;
    Bitmap postImageBbitmap;
    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCreateAccountBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


        binding.CreateBtnCreateAc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.CreateEtName.getText().toString().isEmpty() ||
                        binding.CreateEtEmail.getText().toString().isEmpty() ||
                        binding.CreateEtPassword.getText().toString().isEmpty() || imageUri.toString().isEmpty()) {
                } else {


                    sp = getSharedPreferences("user_name", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp.edit();

                    editor.putString("Name", binding.CreateEtName.getText().toString());
                    editor.putString("password", binding.CreateEtPassword.getText().toString());
                    editor.putString("Email", binding.CreateEtEmail.getText().toString());
//                    editor.putString("ImageUser", imageUri.toString());

                    editor.apply();

                    insert();
                    Intent intent = new Intent(getBaseContext(), LoginActivity.class);
                    startActivity(intent);
                }
            } // end onClick
        }); // end setOnClickListener


        binding.ImageCamira.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(PICK_IMAGE);
            }
        });

    } // end onCreate

    private void insert() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                DatabaseClient.getInstance(getApplicationContext())
                        .getAppDatabase()
                        .userDao()
                        .insert(new User(binding.CreateEtName.getText().toString(),
                                binding.CreateEtEmail.getText().toString(),
                                binding.CreateEtPassword.getText().toString(),
                                imageUri.toString()));

                Log.d("TAG", "run: " + binding.CreateEtEmail.getText().toString());
                Log.d("TAG", "run: " + binding.CreateEtName.getText().toString());
                Log.d("TAG", "run: " + imageUri.toString());

                Log.e("insert", " User Done");
            } // end run()
        }); // end Thread
        thread.start();
    } //end insert()


    public void selectImage(int requestCode) {
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");
        Intent pickIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");
        Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});
        startActivityForResult(chooserIntent, requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) {
            return;
        }
        if (requestCode == PICK_IMAGE) {
            imageUri = data.getData();
            String str = imageUri.toString();
            if (imageUri == null) {
                return;
            }
            final InputStream imageStream;
            try {
                imageStream = getContentResolver().openInputStream(imageUri);
                postImageBbitmap = BitmapFactory.decodeStream(imageStream);
                binding.ImageUser.setImageBitmap(postImageBbitmap);
                binding.CardViewCamira
                        .setVisibility(View.GONE);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
} // end class CreateAccountActivity