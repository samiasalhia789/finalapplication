package com.example.finalapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.finalapplication.database.DatabaseClient;
import com.example.finalapplication.databinding.ActivityLoginBinding;

public class LoginActivity extends AppCompatActivity {
    private ActivityLoginBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


        binding.LoginBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                readAll();

            }
        });


        binding.LoginBtnCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent i = new Intent(LoginActivity.this, CreateAccountActivity.class);
                startActivity(i);
            }
        });
        // end setOnClickListener


    } // end onCreate

    private void readAll() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                boolean user2 = DatabaseClient.getInstance(getApplicationContext())
                        .getAppDatabase()
                        .userDao()
                        .getUser2(binding.loginEtUserEmal.getText().toString().trim(),
                                binding.loginEtPassword.getText().toString().trim());

                if (user2 == true) {
                    Intent i = new Intent(getBaseContext(),MainActivity.class);
                    startActivity(i);
                    finish();

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(LoginActivity.this, "يرجى التأكد من صحة البيانات", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
        thread.start();

    }

} // end class LoginActivity


