package com.example.finalapplication.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.finalapplication.R;
import com.example.finalapplication.databinding.ActivityDetailsOfTheProblemsBinding;
import com.example.finalapplication.modle.ProblemsEntity;

public class DetailsOfTheProblems extends AppCompatActivity {
    private ActivityDetailsOfTheProblemsBinding binding;
    TextView description;
    TextView Title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDetailsOfTheProblemsBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        ProblemsEntity problems = (ProblemsEntity) getIntent().getSerializableExtra("model");
        int position = getIntent().getIntExtra("position", 0);

        int id = problems.getProblemsId();
        String name = problems.getProblemsName();

        description = findViewById(R.id.description);
        Title = findViewById(R.id.Title);


        switch (id) {
            case 0:
                Title.setText("مفهوم المرض النفسي");
                description.setText("\n" +
                        "مصطلح علم النفس يُسْتَهان به عند بعض الأشخاص \n" +
                        "ويطلقون على مصطلح المرض النفسي \"الجنون\" \n" +
                        "ولكن لا يا عزيزي انه ليس كذلك فيعتبر المرض النفسي كباقي الأمراض مثل مرض السكري، الضغط.\n" +
                        "لذلك انت لَسْتُ مجنون ولكن يوجد لديك بعض الأفكار والمعتقدات وربما العادات السيئة تؤثر على تفكيرك وعلى أساليبك و رَدود أفعالك \n" +
                        "فعلم النفس يدعمك تغير تلك العادات وطريقة تفكيرك السلبية ليصبح عقلك أكثر صحة  \n" +
                        "و لذلك جاءت مقولة \"العقل السليم في الجسم السليم\"\n");
                break;
            case 1:
                Title.setText("مكان العلاج");
                description.setText("مكان العلاج يكمن عادة في العيادات الخاصة للصحة النفسية\n" +
                        "تعتقد ان وجود العيادة مثلاً بالقرب من بيت أحد أقاربك أو صديق لك فهنا ترفض الذهاب خشية من أن أحد يراك ولا تريد سماع أي حديث يسئ اليك والى حالتك \n" +
                        "أنت هنا خاطئ ماذا لو رأوك لا تأخذه عائق بالنسبة لك لأنه أمر طبيعي ضع في بالك أنك تريد الراحة فقط ولا تهتم لأحد\n");
                break;
            case 2:
                Title.setText("تكاليف العلاج");
                description.setText("يحتاج العلاج النفسي لبعض التكاليف وهذا ما يجهله بعض الأشخاص \n" +
                        "أولها مجهودك أنت حيث إن ذهبت للعلاج و أنت من الداخل تريد العلاج و الشفاء \n" +
                        "فهذه خطوة رائعة تسرع من عملية العلاج وأيضاً يحتاج العلاج الى بعض الأدوية ولكن في حالات معينه يحددها الطبيب\n");
                break;
            case 3:
                Title.setText("الدواء");
                description.setText("يواجه الطبيب مشكلة عدم تقبل المريض بمرضه و عدم تقبله الدواء و أيضا مشكلة استخدام الادوية بطريقة خاطئة حيث الأدوية النفسة عبارة عن كورسات من 3 , 6  , 12 شهر و ممكن أن يزيد عن ذالك ");
                break;
            case 4:
                Title.setText("اتجاهات الوالدين و الأقارب");
                description.setText("يجهل الوالدين و الأقارب في كيفية التعامل مع المريض مما يؤثر على نفسيته فيعاني الطبيب النفسي مع المريض لان الاجواء المحيطة بالمريض لا تساعد على ذالك فيجب توعية الاهالي بكيفية التعامل مع المريض حتى تتم معالجته بشكل اسرع");
                break;
            case 5:
                Title.setText("وسائل الإحالة");
                description.setText("بعض الأهالي يجبر المريض للذهاب الى العيادة دون تفكير و هذا أسلوب خاطئ يجب على الأهالي زرع في عقله حاجته للعلاج و إقناعه بأهميته بأسلوب هادئ و انهم يريدون رؤيته مرتاح و ليس بالإجبار ");
                break;
            default:
                Log.e("Intent", "error");
        } // end switch

    } // end onCreate
} // end class DetailsOfTheProblems