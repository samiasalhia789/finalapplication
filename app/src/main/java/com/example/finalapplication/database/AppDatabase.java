package com.example.finalapplication.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.finalapplication.interfas.ProblemsDao;
import com.example.finalapplication.interfas.TipsDao;
import com.example.finalapplication.interfas.UserDao;
import com.example.finalapplication.modle.ProblemsEntity;
import com.example.finalapplication.modle.TipsEntity;
import com.example.finalapplication.modle.User;


@Database(entities = {User.class, ProblemsEntity.class, TipsEntity.class}, version = 5)
public abstract class AppDatabase extends RoomDatabase {


    public abstract UserDao userDao();

    public abstract TipsDao tipsDao();

    public abstract ProblemsDao problemsDao();

}