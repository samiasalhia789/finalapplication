package com.example.finalapplication.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.finalapplication.adapter.TipsAdapter;
import com.example.finalapplication.database.DatabaseClient;
import com.example.finalapplication.databinding.FragmentTipsBinding;
import com.example.finalapplication.modle.TipsEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TipsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TipsFragment extends Fragment {

    String CHANNEL_ID = "NEW_CHANNEL_ID5";


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private FragmentTipsBinding binding;

    public TipsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TipsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TipsFragment newInstance(String param1, String param2) {
        TipsFragment fragment = new TipsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = binding.inflate(inflater, container, false);
        View view = binding.getRoot();

        insert();
        TipsFragment.ReadAsync async = new TipsFragment.ReadAsync();
        async.execute();

        return view;
    }

    private void insert() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                DatabaseClient.getInstance(getActivity())
                        .getAppDatabase()
                        .tipsDao()
                        .insert(new TipsEntity("ما ضرّك لو أطفأ هذا العالم أضواءه كلها في وجهك ما دام النور في قلبك متوهجا", 0));

                DatabaseClient.getInstance(getActivity())
                        .getAppDatabase()
                        .tipsDao()
                        .insert(new TipsEntity("يشرق النهار ويبتسم فتبدأ معه الحياة وتعانق خيوطه أرجاء السماء ... فلتشرق أرواحنا كما تشرق الشمس وتملأ الأرض نور ودفء نتعلم منها العطاء بلا حدود ونشكر الله على نعمه التى لا تعد ولا تحصى ونبتسم.", 1));

                DatabaseClient.getInstance(getActivity())
                        .getAppDatabase()
                        .tipsDao()
                        .insert(new TipsEntity("الإستغفار وطن مطمئن متى ما حلقت في سمائه كنت أكثر سعادة وراحة ورضا وأمان ... استغفر الله العظيم الذى لا إله إلا هو الحى القيوم واتوب إليه .", 2));

                DatabaseClient.getInstance(getActivity())
                        .getAppDatabase()
                        .tipsDao()
                        .insert(new TipsEntity("الصباح هو الوجة الأنقى للحياة واللغة الأرقى للكون الواسع ومدخل السعادة والأمل لقلوبنا ... صباح الأمل والتفاؤل .", 3));

                DatabaseClient.getInstance(getActivity())
                        .getAppDatabase()
                        .tipsDao()
                        .insert(new TipsEntity("جميل أن تُعطي من يسألك ما هو  في حاجة إليه والأجمل أن تعطي من لا يسألك وأنت تعرف حاجته!", 4));

                DatabaseClient.getInstance(getActivity())
                        .getAppDatabase()
                        .tipsDao()
                        .insert(new TipsEntity("هناك من يـسكن صفحة واحدة فـي حياتنا ولكنه يجيد الـسكن بها ويملؤها بالطريقة الصحيحة لتصبح بقية الصفحات فارغة.", 5));

                DatabaseClient.getInstance(getActivity())
                        .getAppDatabase()
                        .tipsDao()
                        .insert(new TipsEntity("يقاس مدى وفاء الآخرين بما يفعلوه ويقولوه من وراءك وليس فقط ما تراه أمامك فالكثيرين يستحقون أن يسيروا على السجادة الحمراء كنجوم هوليوود ويحصلون على الأوسكار فى التمثيل!", 6));

                DatabaseClient.getInstance(getActivity())
                        .getAppDatabase()
                        .tipsDao()
                        .insert(new TipsEntity("لا تحكم على أي شخص لمجرد كلام سمعته من شخص آخر !", 7));

                DatabaseClient.getInstance(getActivity())
                        .getAppDatabase()
                        .tipsDao()
                        .insert(new TipsEntity("امنح احزانك الحرية لترحل فحتى الحزن يكره القيود وتأكد انه سيأتى فرح كثير ... ليس لأن الحزن قليل ولكن لأن الله كبير", 8));

                DatabaseClient.getInstance(getActivity())
                        .getAppDatabase()
                        .tipsDao()
                        .insert(new TipsEntity("عبر عن نفسك ومشاعرك دون خجل ولا تخشى أن ينتقدك الأخرين ... إعترف لنفسك بمميزاتك لكن بتواضع  وإبتعد عن الغرور ... فالتواضع هو أساس القوة", 9));

                DatabaseClient.getInstance(getActivity())
                        .getAppDatabase()
                        .tipsDao()
                        .insert(new TipsEntity("لكل صباح حكاية رائعة لا يقراؤها الا من يتذوقها ويرسمها بابتسامة أمل وتفاؤل ... اللهم اجعل هذا الصباح خير وسعادة وتوفيق ... صباح الأمل والتفاؤل .", 10));

                DatabaseClient.getInstance(getActivity())
                        .getAppDatabase()
                        .tipsDao()
                        .insert(new TipsEntity("يقاس مدى وفاء الآخرين بما يفعلوه ويقولوه من وراءك وليس فقط ما تراه أمامك فالكثيرين يستحقون أن يسيروا على السجادة الحمراء كنجوم هوليوود ويحصلون على الأوسكار فى التمثيل!", 11));

                DatabaseClient.getInstance(getActivity())
                        .getAppDatabase()
                        .tipsDao()
                        .insert(new TipsEntity("ويبقى جميلي الروح البسطاء الطيبين العفويين التلقائيّين أكثر جاذبية ولو تجمّل المتصنّعون دهرًا!", 12));


                Log.e("insert2", " Tips Done");
            }
        });
        thread.start();
    }

    class ReadAsync extends AsyncTask<Void, Void, List<TipsEntity>> {
        @Override
        protected List<TipsEntity> doInBackground(Void... voids) {
            List<TipsEntity> tipsEntityList = DatabaseClient.getInstance(getActivity())
                    .getAppDatabase()
                    .tipsDao()
                    .getAll();
            Log.d("TAG2", "doInBackground: " + tipsEntityList.size());
            return tipsEntityList;
        }

        @Override
        protected void onPostExecute(List<TipsEntity> TipsList) {
            super.onPostExecute(TipsList);

            TipsAdapter adapter = new TipsAdapter((ArrayList<TipsEntity>) TipsList, getActivity());

            LinearLayoutManager manager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            binding.TipsRecyclerView.setLayoutManager(manager);
            binding.TipsRecyclerView.setAdapter(adapter);
        }
    }


}