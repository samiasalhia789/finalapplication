package com.example.finalapplication.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;

import com.example.finalapplication.adapter.ProblemsAdapter;
import com.example.finalapplication.database.DatabaseClient;
import com.example.finalapplication.databinding.FragmentProblemsBinding;
import com.example.finalapplication.modle.ProblemsEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProblemsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProblemsFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private FragmentProblemsBinding binding;

    public ProblemsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProblemsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProblemsFragment newInstance(String param1, String param2) {
        ProblemsFragment fragment = new ProblemsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = binding.inflate(inflater, container, false);
        View view = binding.getRoot();
        insert();
        ReadAsync async = new ReadAsync();
        async.execute();


        return view;

    }

    private void insert() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                DatabaseClient.getInstance(getActivity())
                        .getAppDatabase()
                        .problemsDao()
                        .insert(new ProblemsEntity("مفهوم المرض النفسي" ,0));

                DatabaseClient.getInstance(getActivity())
                        .getAppDatabase()
                        .problemsDao()
                        .insert(new ProblemsEntity( "مكان العلاج",1));


                DatabaseClient.getInstance(getActivity())
                        .getAppDatabase()
                        .problemsDao()
                        .insert(new ProblemsEntity("تكاليف العلاج",2));


                DatabaseClient.getInstance(getActivity())
                        .getAppDatabase()
                        .problemsDao()
                        .insert(new ProblemsEntity("الدواء",3));

                DatabaseClient.getInstance(getActivity())
                        .getAppDatabase()
                        .problemsDao()
                        .insert(new ProblemsEntity("اتجاهات الوالدين و الأقارب",4));


                DatabaseClient.getInstance(getActivity())
                        .getAppDatabase()
                        .problemsDao()
                        .insert(new ProblemsEntity("وسائل الإحالة",5));

                Log.e("insert", "Problems Done");
            }
        });
        thread.start();
    }

    class ReadAsync extends AsyncTask<Void,Void,List<ProblemsEntity>>{
        @Override
        protected List<ProblemsEntity> doInBackground(Void... voids) {
            List<ProblemsEntity>problemsEntityList = DatabaseClient.getInstance(getActivity())
                    .getAppDatabase()
                    .problemsDao()
                    .getAll();
            Log.d("TAG", "doInBackground: " + problemsEntityList.size());
            return problemsEntityList;
        }

        @Override
        protected void onPostExecute(List<ProblemsEntity> problemsEntities) {
            super.onPostExecute(problemsEntities);

            ProblemsAdapter adapter = new ProblemsAdapter((ArrayList<ProblemsEntity>) problemsEntities, getActivity());
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
            binding.ProblemsRecyclerView.setLayoutManager(gridLayoutManager);
            binding.ProblemsRecyclerView.setAdapter(adapter);
        }
    }


}