package com.example.finalapplication.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalapplication.databinding.TipsActivityBinding;
import com.example.finalapplication.modle.TipsEntity;

import java.util.ArrayList;

public class TipsAdapter extends RecyclerView.Adapter<TipsAdapter.TipsHolder> {

    ArrayList<TipsEntity> TipsList;


    public TipsAdapter(ArrayList<TipsEntity> classe, FragmentActivity activity) {
        this.TipsList = classe;

    }

    @NonNull
    @Override
    public TipsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TipsActivityBinding binding = TipsActivityBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new TipsHolder(binding);
    } // end onCreateViewHolder

    @Override
    public void onBindViewHolder(@NonNull TipsHolder holder, int position) {
        TipsEntity TipsEntity = TipsList.get(position);
        holder.binding.Tips.setText(TipsEntity.getTipsName());



    } // end  // end onBindViewHolder()

    @Override
    public int getItemCount() {
        return TipsList.size();
    }

    class TipsHolder extends RecyclerView.ViewHolder {

        TipsActivityBinding binding;

        public TipsHolder(TipsActivityBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        } // end TipsHolder()
    } // end TipsHolder


} // end class TipsAdapter
