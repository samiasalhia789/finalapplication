package com.example.finalapplication.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalapplication.activity.DetailsOfTheProblems;
import com.example.finalapplication.databinding.ProblemsActivityBinding;
import com.example.finalapplication.modle.ProblemsEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class ProblemsAdapter extends RecyclerView.Adapter<ProblemsAdapter.ProblemHolder> {

    ArrayList<ProblemsEntity> problems;
    Context context;

    public ProblemsAdapter(ArrayList<ProblemsEntity> classe, Context context) {
        this.problems = classe;
        this.context = context;

    }


    @NonNull
    @Override
    public ProblemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ProblemsActivityBinding binding = ProblemsActivityBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ProblemsAdapter.ProblemHolder(binding);
    } //  end  onCreateViewHolder

    @Override
    public void onBindViewHolder(@NonNull ProblemHolder holder, int position) {
        holder.binding.ProblemsName.setText(problems.get(position).getProblemsName());
        holder.binding.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, DetailsOfTheProblems.class);
                intent.putExtra("model", (Serializable) problems.get(position));
                intent.putExtra("position", position);

                context.startActivity(intent);

                Log.e("getProblemsId", problems.get(position).getProblemsId() + "");
                Log.e("position", position + "");

            } // end onClick()
        }); // end setOnClickListener()
    } // end onBindViewHolder()

    @Override
    public int getItemCount() {
        return problems.size();
    }

    class ProblemHolder extends RecyclerView.ViewHolder {
        ProblemsActivityBinding binding;

        public ProblemHolder(ProblemsActivityBinding binding) {
            super(binding.getRoot());
            this.binding = binding;


        } //end ProblemHolder()

    } // end  class ProblemHolder


} // end class ProblemsAdapter
