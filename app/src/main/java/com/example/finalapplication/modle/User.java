package com.example.finalapplication.modle;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;




@Entity(indices = {@Index(value = {"userEmail"},unique = true)})
public class User {

    @PrimaryKey(autoGenerate = true)
    private int userId;

    @ColumnInfo(name = "userName")
    private String userName;

    @ColumnInfo(name = "userEmail")
    private String userEmail;

    @ColumnInfo(name = "userPassword")
    private String userPassword;


    @ColumnInfo(name = "Image_user")
    private String Image_user;


    public User(String userName, String userEmail, String userPassword,String Image_user) {
        this.userName = userName;
        this.userEmail = userEmail;
        this.userPassword = userPassword;
        this.Image_user = Image_user;
    }


    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getImage_user() {
        return Image_user;
    }

    public void setImage_user(String image_user) {
        Image_user = image_user;
    }


}