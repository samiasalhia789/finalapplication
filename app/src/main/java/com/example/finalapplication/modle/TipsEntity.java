package com.example.finalapplication.modle;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Entity
public class TipsEntity {

    @PrimaryKey(autoGenerate = false)
    private int TipsId;

    @ColumnInfo(name = "userId")
    private int userId;


    @ColumnInfo(name = "TipsName")
    private String TipsName;


    public TipsEntity( String tipsName,int tipsId) {

        this.TipsName = tipsName;
        this.TipsId = tipsId;

    }

    public int getTipsId() {
        return TipsId;
    }

    public void setTipsId(int tipsId) {
        TipsId = tipsId;
    }

    public String getTipsName() {
        return TipsName;
    }

    public void setTipsName(String tipsName) {
        TipsName = tipsName;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }


    public TipsEntity() {
    }


}
