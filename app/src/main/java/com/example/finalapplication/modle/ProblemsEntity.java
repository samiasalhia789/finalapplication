package com.example.finalapplication.modle;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;


@Entity
public class ProblemsEntity implements Serializable {


    @PrimaryKey(autoGenerate = false)
    private int problemsId;


    @ColumnInfo(name = "userId")
    private int userId;


    @ColumnInfo(name = "problemsName")
    private String problemsName;

    public ProblemsEntity(String problemsName , int problemsId) {

        this.problemsName = problemsName;
        this.problemsId = problemsId;



    }

    public int getProblemsId() {
        return problemsId;
    }

    public void setProblemsId(int problemsId) {
        this.problemsId = problemsId;
    }

    public String getProblemsName() {
        return problemsName;
    }

    public void setProblemsName(String problemsName) {
        this.problemsName = problemsName;
    }


    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }


    public int getProblemDetails(int problemsId) {
        return problemsId;
    }


}
