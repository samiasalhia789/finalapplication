package com.example.finalapplication.modle;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class UserWithTips {
    @Embedded
    private User user;
    @Relation(parentColumn = "userId", entityColumn = "userId", entity = TipsEntity.class)
    private List<TipsEntity> TipsList;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<TipsEntity> getTipsList() {
        return TipsList;
    }

    public void setTipsList(List<TipsEntity> TipsList) {
        this.TipsList = TipsList;
    }
}