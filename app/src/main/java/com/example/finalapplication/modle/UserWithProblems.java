package com.example.finalapplication.modle;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class UserWithProblems {
    @Embedded
    private User user;
    @Relation(parentColumn = "userId", entityColumn = "userId", entity = ProblemsEntity.class)
    private List<ProblemsEntity> ProblemsList;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<ProblemsEntity> getProblemsList() {
        return ProblemsList;
    }

    public void setProblemsList(List<ProblemsEntity> ProblemsList) {
        this.ProblemsList = ProblemsList;
    }
}