package com.example.finalapplication.interfas;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.finalapplication.modle.ProblemsEntity;

import java.util.List;

@Dao
public interface ProblemsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ProblemsEntity Problems);


    @Query("select * from ProblemsEntity")
    List<ProblemsEntity> getAll();

    @Delete
    void delete(ProblemsEntity Problems);

    @Update
    void update(ProblemsEntity Problems);


}