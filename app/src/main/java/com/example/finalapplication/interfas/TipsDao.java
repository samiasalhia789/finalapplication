package com.example.finalapplication.interfas;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.finalapplication.modle.TipsEntity;

import java.util.List;

@Dao
public interface TipsDao {
    @Query("select * from TipsEntity")
    List<TipsEntity> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(TipsEntity Tips);

    @Delete
    void delete(TipsEntity Tips);

    @Update
    void update(TipsEntity Tips);


}

