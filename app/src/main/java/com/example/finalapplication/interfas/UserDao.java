package com.example.finalapplication.interfas;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Ignore;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.finalapplication.modle.User;
import com.example.finalapplication.modle.UserWithProblems;
import com.example.finalapplication.modle.UserWithTips;

import java.util.List;

@Dao
public interface UserDao {

    @Query("SELECT * FROM User")
    List<User> getAll();

    @Query("SELECT * FROM User ")
    User getUser();

    @Query("SELECT * FROM User where userEmail  =:a  and  userPassword =:b")
    boolean getUser2(String a, String b);

    @Query("SELECT * FROM User where userEmail like :email")
    User getCurrentUseremail(String email);


    @Query("SELECT * FROM User where userEmail like :password")
    User getPasswordUser(String password);

    @Query("SELECT * FROM ProblemsEntity")
    public List<UserWithProblems> loadUserWithProblems();

    @Ignore
    @Query("SELECT * FROM TipsEntity")
    public List<UserWithTips> loadUserWithTips();

    @Query("SELECT userPassword FROM User where userEmail like :emial")
    String getCurrentPasswordUser(String emial);

    @Insert
    void insert(User user);

    @Delete
    void delete(User user);

    @Update
    void update(User user);


}